
1ml:=~/1ml/1ml ~/1ml/prelude.1ml

# e.g. make descrim
# or make descrim-i for an interactive interpreter..
targets=discrim seal empty ref l-vs-s disjoint-binding
.PHONY: $(targets)

all: $(targets)

define TEMPLATE =
$(1): $(1).1ml
	$$(1ml) $(1).1ml

$(1)-i: $(1).1ml
	$$(1ml) $(1).1ml -
endef

$(foreach target,$(targets),$(eval $(call TEMPLATE,$(target))))
